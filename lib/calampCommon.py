import os, re, telnetlib, time

PORT = 5661
SBD_PORT = 6291
user = "root"
password = "p30r1a"
HOST = '192.168.1.40'


def initializetelnet(HOST):
    retries = 3
    while retries != 0:
        try:
            tn = telnetlib.Telnet(HOST,PORT)
            output = tn.read_until(b"ready",5)
            if re.search(b"ready",output):
                retries = 0
            else:
                print("telnet not showing response messages. Retrying...")
                retries = retries - 1
                if 'tn' in globals():
                    tn.close()
        except Exception as e:
            print("Telnet refused connection. Waiting 5 seconds to retry...")
            retries = retries - 1
            if retries == 0:
                return -1
            time.sleep(5)
    return tn

def initializeSBD(HOST):
    retries = 3
    while retries != 0:
        try:
            tn = telnetlib.Telnet(HOST,SBD_PORT)
            retries = 0
        except Exception as e:
            print("Telnet refused connection. Waiting 5 seconds to retry...")
            retries = retries - 1
            if retries == 0:
                return -1
            time.sleep(5)
    return tn

def paramikoConnect(hname, key = None):
    import paramiko
    password = 'p30r1a'
    username = "root"
    try:
        client = paramiko.SSHClient()
        # client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hname, port=22, username=username, password=password, key_filename = key)
        print('paramiko client connected')
        return client
    except Exception as e:
        print("Paramiko error: " + str(e))
        return -1

class Info:
    """
    Parses the rawInput from the Gen2 telnet into param and info

    parse()
        splits rawInput into param and info according to substring

    """
    def __init__(self, rawInput):
        self.rawInput = rawInput
        self.param = ""
        self.info = ""
        self.parse()

    def parse(self):
        line = self.rawInput.split('=')
        self.param = line[0].replace("\r\n","").strip('200-')
        if len(line) > 1: self.info = line[1].strip()