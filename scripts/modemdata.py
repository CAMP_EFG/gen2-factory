"""
Example:
    python modemdata.py

    200- *************     Modem Diagnostics    *************
    200-    Access technology   =  GSM/UMTS
    200-    RSSI                =  14
    200-    RSSI in dBm         =  -85
    200-    ERI                 =  Roaming
    200-    NetworkType         =  UMTS Service
    200-    EC/Io               =  -2.5
    200-    RTC DATE & TIME     =  Unknown
    200-    FIRMWARE REV        =  03.320
    200-    MDN                 =  0000005621
    200-    MEID (CDMA2000)     =  99000218985797
    200-    IMEI (GSM/UMTS)     =  990002189857970
    200-    ESN                 =  990002189857970
    200-    MCC                 =  310
    200-    MNC                 =  260
    200-    IMSI (GSM/UMTS)     =  204043720022125
    200     ICCID (GSM/UMTS)    =  89314404000048889373
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################

def modemdata():
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"modemdata")
    array = []

    while True:
        output = tn.expect([b"200"], 0.1)
        if b"200" not in output[2]: return
        output += tn.expect([b"\r\n"], 1)
        line = output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b"")
        line = line.decode('ascii')
        print(line)
        array.append(Info(line))
    tn.close()

    return array

if __name__ == "__main__":
    if modemdata() == -1: raise Exception("modemdata fail")