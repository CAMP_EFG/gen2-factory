"""
Example:
    python setserialno.py -v 2342123411

    setserialno 2342123411
    setlevel debug passed

    200 OK
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setserialno(value):
    tn = initializetelnet(HOST)
    print("\nsetserialno " + value)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setserialno " + value.encode())
    output = tn.expect([b"200 OK"], 1)
    if b"200 OK" not in output[2]:
        tn.write(b"setserialno " + value.encode())
        while True:
            output = tn.expect([b"500"], 0.1)
            if b"500" not in output[2]: break
            output += tn.expect([b"\r\n"], 1)
            print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    else: print(output[2].decode('ascii').replace("\b", "").replace("\r\n", ""))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    if setserialno(args.value) == -1: raise Exception("setserialno error")