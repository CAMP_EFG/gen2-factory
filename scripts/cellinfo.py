"""
Example:
python cellinfo.py

200-ESN="990002189857970"
200-MEID="99000218985797"
200-IMEI="990002189857970"
200-Channel=0
200-Band=N/A
200-RSSI=-83
200-Firmware="03.320"
200-Roaming="Roaming"
200-Service="UMTS Service"
200-Type="UMTS"
200-IMSI="204043720022125"
200-MCC=310
200-MNC=260
200-ICCID="89314404000048889373"
200 MDN="0000005621"
"""
import os, sys, argparse, re, time

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def cellinfo():
	tn = initializetelnet(HOST)
	print('Waiting 120 seconds...')
	time.sleep(120)			#cquinones increased wait time, need 120 sec minimum
	tn.write(b"cellinfo")
	result = ""
	array = []
	while True:
		output = tn.expect([b"200"], 0.5)
		if b"200" not in output[2]: break
		output += tn.expect([b"\r\n"], 1)
		currentLine = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentLine)
		result += currentLine + "\n"
		formatted = Info(currentLine)
		array.append(formatted)

	tn.close()
	return array

if __name__ == "__main__":
	cellinfo()
