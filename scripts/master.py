import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def autoreconnect(value1 = None, value2 = None, value3 = None, value4 = None):
    '''
    Description: Turn on to auto reconnect
    Example:
        python autoreconnect.py -v on
    Response:
            Success: 200 on
            Fail: 500 Syntax Error - Usage: autoreconnect <on | off>
    '''
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if not re.search(b"debug",output):
        print("setlevel debug FAILED")
        return -1
    if value1 == "on":
        tn.write(b"autoreconnect " + value1.encode() + value2.encode() + value3.encode() + value4.encode())
        output = tn.expect([b"200 on"], 2)
        if b"200 on" not in output[2]: return -1
        output += tn.expect([b"\r\n"], 5)
        print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    elif value1 == "off":
        tn.write(b"autoreconnect " + value1.encode() + value2.encode() + value3.encode() + value4.encode())
        output = tn.expect([b"200 off"], 2)
        if b"200 off" not in output[2]: return -1
        output += tn.expect([b"\r\n"], 5)
        print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))

    else:
        tn.write(b"autoreconnect " + value1.encode())
        while True:
            output = tn.expect([b"500"], 0.1)
            if b"500" not in output[2]: break
            output += tn.expect([b"\r\n"], 1)
            print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))

    tn.close()

    return 0