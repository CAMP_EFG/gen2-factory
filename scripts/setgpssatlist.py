"""
Example:
    python setgpssatlist.py -v 5,19,22

    setgpssatlist 5,19,22
    setlevel debug passed

    200 Valid GPS List has been set
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setgpssatlist(value):
    tn = initializetelnet(HOST)
    print("\nsetgpssatlist " + value)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setgpssatlist " + value.encode())
    output = tn.expect([b"200 Valid GPS"], 2)
    if b"200 Valid GPS" not in output[2]: return -1
    else:
        output += tn.expect([b"\r\n"], 5)
        print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    if setgpssatlist(args.value) == -1: raise Exception("setgpssatlist error")