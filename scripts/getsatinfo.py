"""
Example:
    python getsatinfo.py

    getsatinfo

    200-Mode=SBD
    200-CSQ=0
    200-Averages=5
    200-Antennas=Missing (2425 mV)
    200-IMEI=300125060170950
    200 ICCID=8988169555000522605
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################

def getsatinfo():
    try:
        print("\ngetsatinfo")
        tn = initializetelnet(HOST)
        tn.write(b"getsatinfo")
        result = ""
        i = 0
        array = []
        while i < 6:
            output = tn.expect([b"200"], 3)
            output += tn.expect([b"\r\n"], 5)
            currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
            print(currentline)
            result += currentline
            array.append(Info(currentline))
            i += 1

        tn.close()
        return array

    except Exception as e:
        return -1

if __name__ == "__main__":
	getsatinfo()