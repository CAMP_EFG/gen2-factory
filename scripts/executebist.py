"""
Execute Built-In Self Test
"""
from calampCommon import initializetelnet, HOST
import re, argparse

def executebist(value1 = None, value2 = None, value3 = None, value4 = None):
    tn = initializetelnet(HOST)
    print("\nexecutebist " + str(value1) + " " + str(value2) + " " + str(value3) + " " + str(value4))
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"executebist " + value1.encode() + value2.encode() + value3.encode() + value4.encode())

    tn.close()

    print("OK")
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - CommandValue1')
    parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - CommandValue2')
    parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - CommandValue3')
    args = parser.parse_args()
    if executebist(args.value, args.value1, args.value2, args.value3) == -1: raise Exception("executebist error")