import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################

def setdialnum():
    print("Setting dial number to 12345678912...")
    tn.write(b"setdialnum 12345678912")
    output = tn.read_until(b"OK",30)
    if re.search(b"OK",output):
        print("OK response received good")
        print("Verifying dial number was set with waninfo...")
        tn.write(b"waninfo")
        output = tn.read_until(b"Daemon",30)
        if re.search(b"12345678912",output):
            print("waninfo shows 12345678912 good")
            print("Setting dialnum back to {0}".format(dialnum))
            tn.write(b"setdialnum "+dialnum)
            output = tn.read_until(b"OK",20)
            if re.search(b"OK",output):
                print("OK response received good")
                print("Verifying dial number was set with waninfo...")
                tn.write(b"waninfo")
                output = tn.read_until(b"Daemon",20)
                if re.search(dialnum,output):
                    print("dial number was set back to {0} good".format(dialnum))
                else:
                    print("dial number was not set back to {0} FAILED".format(dialnum))
            else:
                print("OK response was not received FAILED")
        else:
            print("waninfo does not show 12345678912 FAILED")
    else:
        print("OK response was not received FAILED")
    tn.close()

def getdialnum():
    global tn, dialnum
    dialnum = "none"
    print("\nsetdialnum Test")
    tn = initializetelnet(HOST)
    print("Capturing dialnum with waninfo...")
    tn.write(b"waninfo")
    output = tn.read_until(b"TelNum=", 5)
    if not re.search(b"TelNum=",output):
        print("waninfo did not give a response")
        return
    else:
        dialnum = tn.read_until(b"\n",20).strip()
        if len(dialnum) == 11 and dialnum.isdigit():
            print("Dial number = {0} good".format(dialnum))
            setdialnum()
        else:
            print("Dial Number = {0} FAILED".format(dialnum))

if __name__ == "__main__":
    getdialnum()
