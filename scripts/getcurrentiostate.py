"""
Example:
    python getcurrentiostate.py

    200-STG1 State = Active
    200-STG2 State = Active
    200-STG3 State = Active
    200-STG4 State = Active
    200-RTERM State = 0
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getcurrentiostate():
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"getcurrentiostate")
    output = tn.expect([b"STG1 State"], 10)
    output += tn.expect([b"\r\n"], 5)
    result = ""
    array = []

    line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(line)
    result += line + "\n"
    array.append(Info(line))

    output = tn.expect([b"STG2 State"], 10)
    output += tn.expect([b"\r\n"], 5)
    line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(line)
    result += line + "\n"
    array.append(Info(line))

    output = tn.expect([b"STG3 State"], 10)
    output += tn.expect([b"\r\n"], 5)
    line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(line)
    result += line + "\n"
    array.append(Info(line))

    output = tn.expect([b"STG4 State"], 10)
    output += tn.expect([b"\r\n"], 5)
    line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(line)
    result += line + "\n"
    array.append(Info(line))

    output = tn.expect([b"RTERM State"], 10)
    output += tn.expect([b"\r\n"], 5)
    line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(line)
    result += line + "\n"
    array.append(Info(line))

    tn.close()

    return array

if __name__ == "__main__":
    if getcurrentiostate() == -1: raise Exception("getcurrentiostate fail")
