"""
    Description: Get bist result
    Example:
        python getbistresult.py

        200-DDR RAM Test=PASS
        200-Ethernet LAN Test=PASS
        200-USB Host Interface Test=PASS
        200 SPI Test=PASS
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getbistresult():
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"getbistresult")
    output = tn.expect([b"DDR RAM Test"], 10)
    output += tn.expect([b"\r\n"], 5)
    result = ""
    array = []

    currentLine = (output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    print(currentLine)
    result += currentLine + "\n"
    array.append(Info(currentLine))

    output = tn.expect([b"Ethernet LAN Test"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentLine = (output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    print(currentLine)
    result += currentLine + "\n"
    array.append(Info(currentLine))

    output = tn.expect([b"USB Host Interface"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentLine = (output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    print(currentLine)
    result += currentLine + "\n"
    array.append(Info(currentLine))

    output = tn.expect([b"SPI Test"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentLine = (output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    print(currentLine)
    result += currentLine + "\n"
    array.append(Info(currentLine))

    tn.close()

    return array

if __name__ == "__main__":
    if getbistresult() == -1: raise Exception("getbistresult fail")