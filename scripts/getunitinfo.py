"""
    Example:
        python getunitinfo.py

        GETUNITINFO TEST

        200-UNIT_MODEL=PL631e
        200-Unit HW P/N=5441881-00
        200-Unit Serial Number=2857S002YC
        200-Current LAN MAC Address=00:0A:75:09:B7:7E
        200-Current LAN IP Address=192.168.1.40
        200-LAN MAC Address=00:0A:75:09:B7:7E
        200-LAN IP Address=192.168.1.40
        200-Application Version=CAGEN2-v1.0.2.65-2-g483462eb
        200-Boot Version=U-Boot 2013.04-rc2-CAGEN2-v1.0.2.9 (Jul 11 2017 - 11:14:41)
        200-Kernel Version=3.9.0-rc6-CAGEN2-v1.0.2.16
        200 Kernel Build Date=#7 Tue Oct 30 10:08:56 CDT 2018

    Return: An array of classes containing param and info
    """
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################



def getunitinfo():

    try:
        tn = initializetelnet(HOST)
        #print("\nGETUNITINFO TEST")
        tn.write(b"setlevel debug")
        tn.read_until(b"debug",1)
        tn.write(b"getunitinfo")
        result = ""
        array = []
        while True:
            output = tn.expect([b"200"], 0.1)
            if b"200" not in output[2]:
                break
            output += tn.expect([b"\r\n"], 1)
            currentLine = (output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b"")).decode('ascii')
            print(currentLine)
            result += currentLine.replace("\r\n", "") + "\n"
            formatted = Info(currentLine)
            array.append(formatted)

        tn.close()
        return array

    except Exception as e:
        return -1

if __name__ == "__main__":
	getunitinfo()


def getIPAddr():
	""" Return IP address from getunitinfo """
	unitinfo = getunitinfo()
	ipaddr = unitinfo[4].info
	return ipaddr

def getHWPartnum():
	""" Return the hardware part numbr from getunitinfo """
	unitinfo = getunitinfo()
	partnum = unitinfo[1].info
	return partnum

def getFW():
	""" Return the Application Version (Firmware) from getunitinfo """
	unitinfo = getunitinfo()
	fw = unitinfo[7].param, unitinfo[7].info
	print(fw)
	return fw

def getCOProc():
	""" Return the Boot Version (Coprocessor) from getunitinfo """
	unitinfo = getunitinfo()
	coproc = unitinfo[8].info
	return coproc


def pullunitinfo(line, point):
	""" Return a specific line from getunitinfo """
	tn = initializetelnet(HOST)
	#print("==> pull unit info")
	tn.write(b"setlevel debug")
	tn.read_until(b"debug",1)
	tn.write(b"getunitinfo")

	int = 0
	line += 1
	while int < line:
		output = tn.expect([b"200"], 0.1)
		if b"200" not in output[2]: return
		output += tn.expect([b"\r\n"], 1)
		#print(output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b""))
		int += 1

	tn.close()
	temp = output[5].replace(b"\b", b"").replace(b"\r\n", b"")
	info = temp[point:]
	return info.decode('utf-8')