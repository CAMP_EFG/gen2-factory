"""
Example: python waninfo.py
waninfo

200-WAN=DOWN

"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def waninfo():
    print("waninfo")

    tn = initializetelnet(HOST)

    tn.write(b'waninfo\r\n')

    output = tn.expect([b"201"], 5)
    up = False
    if "201" not in str(output):
        tn.write(b"waninfo")
        output = tn.expect([b"200"], 120)
        if "200" not in str(output):
                print("Waninfo fail")
                return -1
    if "201" in str(output): up = True

    output += tn.expect([b"\r\n"])
###############################################################################################
    array = []
    tn.close()
    tn = initializetelnet(HOST)
    tn.write(b"waninfo")
    if up:
        output = tn.expect([b"201"], 0.1)
        output += tn.expect([b"\r\n"], 1)
        line = output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b"")
        line = line.decode('ascii')
        print(line)
        array.append(Info(line))

        output = tn.expect([b"201"], 0.1)
        output += tn.expect([b"\r\n"], 1)
        line = output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b"")
        line = line.decode('ascii')
        print(line)
        array.append(Info(line))
    else:
        output = tn.expect([b"200"], 0.1)
        if b"200" not in output[2]: return
        output += tn.expect([b"\r\n"], 1)
        line = output[2].replace(b"\b", b"") + output[5].replace(b"\b", b"").replace(b"\r\n", b"")
        line = line.decode('ascii')
        print(line)
        array.append(Info(line))
        tn.close()

    return array


if __name__ == "__main__":
    waninfo()
