"""
Example:
    python setunithwno.py -v 2312321-55

    setunithwno 2312321-55
    setlevel debug passed
    200 OK

Fail: python setunithwno.py -v 2312321-5

setunithwno 2312321-5
setlevel debug passed
500-Invalid Unit Serial Number. Syntax: setserialno XXXYWZZZAA
500-          where, XXX is the day of the year,
500-          Y is the last digit of the year,
500-          W is the supplier code (S for CalAmp),
500-          ZZZ is the serial number for the day,
500-          AA is the product code (YD for Cell Radio, YC for Sat Radio)
500           Example Syntax: setserialno 2053S032YD
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setunithwno(value):
    tn = initializetelnet(HOST)
    print("\nsetunithwno " + value)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setunithwno " + value.encode())
    output = tn.expect([b"200 OK"], 1)
    if b"200 OK" not in output[2]:
        tn.write(b"setserialno " + value.encode())
        while True:
            output = tn.expect([b"500"], 0.1)
            if b"500" not in output[2]: break
            output += tn.expect([b"\r\n"], 1)
            print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    else: print(output[2].decode('ascii').replace("\b", "").replace("\r\n", ""))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    if setunithwno(args.value) == -1: raise Exception("setunithwno error")