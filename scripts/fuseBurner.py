"""
Author: Joshua Liew
Description: fuseBurner script upgraded from original fuseBurnerV5.py script which runs on Python27
Date Modified: 6/14/2019
"""

import os, sys, argparse, re, time, datetime

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info, paramikoConnect
##########################################################################################

# global Variables
targetIP = '192.168.1.40'
LOG_PATH = "C:\\Temp\\Logs\\"
MSG_PATH = "C:\\Temp\\Messages\\"
HWNO = "4402104-17"
SSH = ""
deviceType = "cellAir"
antenna = "internal"

class sshSession:

	def __init__(self,address):

		# kick off paramiko process, passing in ip as parameter
		self.client = paramikoConnect(address)

	def installer(self):
		print("Installing Cat firmware...")
		stdin, stdout, stderr = self.client.exec_command(b"installer.sh cat\n")
		line = stdout.read()
		if re.search("/*/*/* Complete /*/*/*",line):
			print("Installation was successful OK")
			return True
		elif re.search("SUCCESS.*Flashed u-boot\.sb",line):
			print("u-boot FLASHED OK")
		elif re.search("SUCCESS.*Flashed uImage",line):
			print("uImage FLASHED OK")
		elif re.search("SUCCESS.*CAT apps",line):
			print("CAT apps FLASHED OK")
		elif re.search("Aborting|Unable|Failed|No.*found|Error",line) and not re.search("try tftp",line):
			print("400 Installation FAILED")
			return False

	def calamp_mpi(self):
		global antenna, deviceType
		print("checking calamp_mpi values as an {0} antenna {1}...".format(antenna,deviceType))
		stdin, stdout, stderr = self.client.exec_command("export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/calamp/lib/ && /opt/calamp/sbin/calamp_mpi")

		line = stdout.read().decode('ascii')
		if re.search("HW",line):
			devicehwno = line.split("=")[1].strip().split('\n')[0]
			print("Unit HW P/N = {0}".format(devicehwno))
			if devicehwno != HWNO:
				print("Expected Hardware Number of {0} and Actual Device Hardware Number showed {1}".format(HWNO,devicehwno))
				return -1

		if re.search("MAC",line):
			macaddr = line.split("=")[5].strip().split('\n')[0]
			print("LAN MAC Address = {0}".format(macaddr))
			if macaddr[0:8] != "00:0A:75":
				print("Invalid MAC Address")
				return -1
		if re.search("Serial",line):
			serialno = line.split("=")[6].strip().split('\n')[0]
			print("Unit Serial Number = {0}".format(serialno))
			if deviceType == "cellAir" and len(serialno) == 10 and serialno.isalnum():
				if serialno[8:10] != "YD":
					if serialno[8:10] != "YG":
						if serialno[8:10] != "YN":
							print("Serial Number does not show cellAir")
							return -1
			elif deviceType == "satAir" and len(serialno) == 10 and serialno.isalnum():
				if serialno[8:10] != "YC":
					if serialno[8:10] != "YH":
						print("Serial Number does not show satAir")
						return -1
			else:
				print("Invalid serial number")
				return -1
			if serialno[4] != "S":
				print("Serial number does not show calAmp")
				return -1

		stdin, stdout, stderr = self.client.exec_command("export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/calamp/lib/ && /opt/calamp/sbin/calamp_env -r UNIT_MODEL")

		read_modelno = True

		line = stdout.read().decode('ascii')
		if re.search("PL6",line):
			modelno = line.strip()
			print("Unit Model Number = {0}".format(modelno))
			if deviceType == "cellAir" and modelno[3] != "4":
				print("Model Number does not show cellAir")
			elif deviceType == "satAir" and modelno[3] != "3":
				print("Model Number does not show satAir")
			if antenna == "external" and modelno[5] != "e":
				print("Model Number does not show external antenna")
			elif antenna == "internal" and modelno[5] != "i":
				print("Model Number does not show internal antenna")
			read_modelno = False

def fuseBurner():
	global targetIP
	client = paramikoConnect(targetIP)
	client.close()

def installContentCheck():
	fc = os.popen("dir C:\inetpub\wwwroot\install").read()
	contentCheck = ["manifest","SKIProvisioning","SKIServiceDashboard","u-boot.sb","uImage"]
	for value in contentCheck:
		if re.search(value,fc):
			print("{0} was found in install directory OK".format(value))
		else:
			return value
	return 0

def clearboot():
	global SSH
	print("Checking for files in boot directory...")
	stdin, stdout, stderr = SSH.client.exec_command("ls /boot")
	if "manifest.txt" in str(stdout.read()):
		print("Files were found in /boot directory. Clearing boot directory...")
		stdin, stdout, stderr = SSH.client.exec_command("rm /boot/*")
	else:
		print("Files were not found in /boot directory. OK")

def stopApps():
	global SSH
	print("Stopping CalAmp applications...")
	stdin, stdout, stderr = SSH.client.exec_command("/opt/calamp/etc/init.d/S01mmgr stop")
	if "power" in str(stdout.read()):
		print("CalAmp applications were stopped OK")
	else:
		return -1


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-hwNo',  dest='HWNO',  default = '', type=str,  help='hwNo - Hardware Number')
	args = parser.parse_args()

	if args.HWNO[0:7] == "4427199":
		deviceType = "satAir"
	if args.HWNO[0:7] == "5441881":
		deviceType = "satAir"
	if args.HWNO[0:7] == "4402104":
		antenna = "internal"
	if args.HWNO[0:7] == "5555509":
		antenna = "internal"
	if args.HWNO[0:7] == "4402105":
		antenna = "external"
	if args.HWNO[0:7] == "5555510":
		antenna = "external"

	# begin SSH session
	global SSH
	SSH = sshSession(targetIP)

	# validate hardware
	SSH.calamp_mpi()

	# ensure the installer content is OK
	#TODO check use tftp or wget
	# if installContentCheck() != 0: raise Exception("install content check fail")

	# cleanup /boot directory
	clearboot()

	# stops calAmp applications to release /opt directory
	stopApps()

	print("creating backup of u-boot in /root directory...")
	stdin, stdout, stderr = SSH.client.exec_command("dd if=/dev/mtd4 of=/root/u-boot-calamp.img")

	# close ssh client
	SSH.client.close()

if __name__ == "__main__":
	main()