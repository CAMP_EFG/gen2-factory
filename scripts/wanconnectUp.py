"""
    python wanconnectUp.py

    wanconnect up

    200 Ok
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def wanconnectUp():
    try:
        print("\nwanconnect up")
        tn = initializetelnet(HOST)
        tn.write(b"wanconnect up")
        output = tn.expect([b"200 Ok"], 10)
        print(output[2].decode('ascii').replace("b", ""))
        tn.close()
    except Exception as e:
        print("Received error: " + str(e))
        print("wanconnect up error")

if __name__ == "__main__":
    wanconnectUp()