"""
Example:
    python readpowermonitorstate.py

    Main Power State= ON passed
    Batt Backup State= OFF passed
    RTC State= OFF passed
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def readpowermonitorstate():
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"readpowermonitorstate")
    tn.read_until(b"Main Power State",1)
    output = tn.read_until(b"\n",1)
    if re.search(b"ON",output):
        print("Main Power State= ON passed")
    elif re.search(b"OFF",output):
        print("Main Power State= OFF passed")
    else:
        print("Main Power State not ON or OFF = FAILED")
    output = tn.read_until(b"Batt Backup State",1)
    output = tn.read_until(b"\n",1)
    if re.search(b"ON",output):
        print("Batt Backup State= ON passed")
    elif re.search(b"OFF",output):
        print("Batt Backup State= OFF passed")
    else:
        print("Batt Backup State not ON or OFF = FAILED")
    output = tn.read_until(b"RTC State",1)
    output = tn.read_until(b"\n",1)
    if re.search(b"ON",output):
        print("RTC State= ON passed")
    elif re.search(b"OFF",output):
        print("RTC State= OFF passed")
    else:
        print("RTC State not ON or OFF = FAILED")
    tn.close()

def main():
    readpowermonitorstate()

if __name__ == "__main__":
    main()
