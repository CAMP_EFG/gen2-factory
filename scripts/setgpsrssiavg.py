"""
Example:

    python setgpsrssiavg.py -v 1
    setgpsrssiavg
    setlevel debug passed

    200 RSSI Avg count has been set
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setgpsrssiavg(value):
    tn = initializetelnet(HOST)
    print("setgpsrssiavg" )
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setgpsrssiavg " + value.encode())
    output = tn.expect([b"200"], 10)
    output += tn.expect([b"\r\n"], 5)
    print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))

    #TODO: continue after expected results known
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    setgpsrssiavg(args.value)