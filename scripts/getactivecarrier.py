"""
Description: Get active carrier
Example:
    python getactivecarrier.py

    getactivecarrier

    200 GSM=Generic/GSM/Global
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getactivecarrier():
	try:
		print("\ngetactivecarrier")
		tn = initializetelnet(HOST)
		tn.write(b"getactivecarrier")
		output = tn.expect([b"200 GSM"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentLine = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "")
		print(currentLine)
		tn.close()
		return Info(currentLine)
	except Exception as e:
		print("Received error: " + str(e))
		print("getactivecarrier error")

if __name__ == "__main__":
	getactivecarrier()