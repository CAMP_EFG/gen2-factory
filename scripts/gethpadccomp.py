    """
    Desciption: Get High Precision ADC Compensation
    Example:
        python gethpadccomp.py

        200-VBAT=0.00,0,
        200-VBAKBAT=1255.00,1255,
        200
    """
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def gethpadccomp():
    try:
        tn = initializetelnet(HOST)
        tn.write(b"setlevel debug")
        output = tn.read_until(b"debug",1)
        tn.write(b"gethpadccomp")
        output = tn.expect([b"VBAT"], 10)
        output += tn.expect([b"\r\n"], 5)
        result = ""
        array = []

        currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
        print(currentline)
        result += currentline + "\n"
        array.append(Info(currentline))

        output = tn.expect([b"VBAKBAT"], 10)
        output += tn.expect([b"\r\n"], 5)
        currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
        print(currentline)
        result += currentline + "\n"
        array.append(Info(currentline))

        tn.close()

        return array
    except Exception as e:
        return -1

if __name__ == "__main__":
    if gethpadccomp() == -1: raise Exception("gethpadccomp fail")
