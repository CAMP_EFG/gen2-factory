"""
Example:
    python getnetworkmode.py

    getnetworkmode

    200 ROAMING=WorldWideRoaming
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getNetworkMode():
    try:
        print("\ngetnetworkmode")
        tn = initializetelnet(HOST)
        tn.write(b"getnetworkmode")
        output = tn.expect([b"200 ROAMING"], 10)
        output += tn.expect([b"\r\n"], 5)
        result = ""

        currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "")
        print(currentline)
        result += currentline.replace("\r\n", "") + "\n"

        tn.close()
        return Info(currentline)

    except Exception as e:
        return -1

if __name__ == "__main__":
    getNetworkMode()