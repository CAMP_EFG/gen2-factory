"""
Example:
    python getgpsinfo.py

    200-GPS Fix=NONE
    200-Lat and Long=0.000,0.000
    200-Date and Time in UTC=0.000
    200-Number of Satellites Found=1
    200-Ground Speed=0.000
    200-List of Satellites Found with C/No=46,36
    200-C/No=51=0,46=36,33=0,41=0,16=0,22=0
    200-Averages=1
    200 Antennas=Missing (2989 mV, 2989 mV)
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getgpsinfo():
	try:
		tn = initializetelnet(HOST)

		tn.write(b"getgpsinfo")
		output = tn.expect([b"GPS Fix"], 10)
		output += tn.expect([b"\r\n"], 5)
		result = ""
		array = []

		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Lat and Long"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Date and Time"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Number of Satellite"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Ground Speed"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"List of Satellites Found with C/No"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"C/No"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Averages"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		output = tn.expect([b"Antennas"], 10)
		output += tn.expect([b"\r\n"], 5)
		currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
		#print(currentline)
		result += currentline + "\n"
		array.append(Info(currentline))

		tn.close()
		return array

	except Exception as e:
		return -1

if __name__ == "__main__":
    if getgpsinfo() == -1: raise Exception("getgpsinfo fail")
