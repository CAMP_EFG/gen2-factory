"""
Example:
    python modemreset.py
    MODEM RESET TEST
    Modem will now reset... passed
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, telnetlib, PORT
##########################################################################################


def telnet():
    print("MODEM RESET TEST")
    try:
        tn = telnetlib.Telnet(HOST, PORT)
    except:
        print("telnet connection was refused")
        return
    tn.write(b"modemreset")
    output = tn.read_until(b"Modem will now reset...",1)
    if re.search(b"Modem will now reset...",output):
        print("Modem will now reset... passed")
    else:
        print("Modem will now reset... FAILED")

if __name__ == "__main__":
   telnet()