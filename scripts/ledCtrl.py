"""
Description: Get bist result
Example:
    python ledCtrl.py -v all -v1 off

    ledCtrl all off

    200 Set successfully
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def ledCtrl(value1 = None, value2 = None, value3 = None, value4 = None):

    tn = initializetelnet(HOST)
    print("\nledCtrl " + str(value1) + " " + str(value2) + " " + str(value3) + " " + str(value4))
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"ledCtrl " + value1.encode() + b" " +  value2.encode())
    output = tn.expect([b"200"], 2)
    output += tn.expect([b"\r\n"], 2)
    if b"Set successfully" not in output[5]: return -1
    else: print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
    parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
    parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
    args = parser.parse_args()
    if ledCtrl(args.value, args.value1, args.value2, args.value3) == -1: raise Exception("ledCtrl error")