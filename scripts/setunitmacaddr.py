"""
Example:
python setunitmacaddr.py -v 23:56:12:12:12:12

setunitmacaddr 23:56:12:12:12:12
setlevel debug passed
200 OK

Fail:
python setunitmacaddr.py -v 23:56:12:12:12:1

setunitmacaddr 23:56:12:12:12:1
setlevel debug passed
500-Invalid MAC address. Syntax: setunitmacaddr XX:XX:XX:XX:XX:XX
500  "X" can be any hexadecimal value.
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setunitmacaddr(value):
    tn = initializetelnet(HOST)
    print("\nsetunitmacaddr " + value)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setunitmacaddr " + value.encode())
    output = tn.expect([b"200 OK"], 2)
    if b"200 OK" not in output[2]:
        tn.write(b"setunitmacaddr " + value.encode())
        while True:
            output = tn.expect([b"500"], 0.1)
            if b"500" not in output[2]: break
            output += tn.expect([b"\r\n"], 1)
            print(output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", ""))
    else: print(output[2].decode('ascii').replace("\b", "").replace("\r\n", ""))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    if setunitmacaddr(args.value) == -1: raise Exception("setunitmacaddr error")