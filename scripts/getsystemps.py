"""
Description: Get System Monitor Temperature
Example:
    python getsystemps.py

    200 Router Temp=TBDSat DB Temp=TBD
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################


def getsystemps():
    tn = initializetelnet(HOST)
    print("\ngetsystemps")
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"getsystemps")
    output = tn.expect([b"Router Temp="], 10)
    output += tn.expect([b"\r\n"], 5)
    currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(currentline)

    tn.close()

    return Info(currentline)

if __name__ == "__main__":
    if getsystemps() == -1: raise Exception("getsystemps fail")
