"""
    Example:
    python setunitmodelno.py -v PL641e

    setunitmodelno PL641e
    setlevel debug passed

    200 OK
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def setunitmodelno(value):
    tn = initializetelnet(HOST)
    print("\nsetunitmodelno " + value)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    if re.search(b"debug",output):
        print("setlevel debug passed")
    else:
        print("setlevel debug FAILED")
        return -1
    tn.write(b"setunitmodelno " + value.encode())
    output = tn.expect([b"200 OK"], 2)
    if b"200 OK" not in output[2]: return -1
    else: print(output[2].decode('ascii'))
    tn.close()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    if setunitmodelno(args.value) == -1: raise Exception("setunitmodelno error")