"""
Example:
    python getsystimedate.py

    200 CCLK="19/02/13,22:02:55,-24"
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################

def getsystimedate():
    try:
        tn = initializetelnet(HOST)
        tn.write(b"getsystimedate")
        output = tn.expect([b"200 CCLK"], 10)
        output += tn.expect([b"\r\n"], 5)
        result = ""
        line = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "")
        print(line)
        result += line + "\n"
        tn.close()
        return Info(line)
    except Exception as e:
        return -1

if __name__ == "__main__":
    getsystimedate()