"""
    Desciption: Get High Precision ADC Info
    Example:
        python gethpadcinfo.py

        200-VBAT=1255.00,1255
        200-VBAKBAT=170.00,170
        200-VREF=1697.00,1697
        200 VGND=0.00,0
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST, Info
##########################################################################################

def gethpadcinfo():
    tn = initializetelnet(HOST)
    tn.write(b"setlevel debug")
    output = tn.read_until(b"debug",1)
    tn.write(b"gethpadcinfo")
    output = tn.expect([b"VBAT"], 10)
    output += tn.expect([b"\r\n"], 5)
    result = ""
    array = []

    currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(currentline)
    result += currentline + "\n"
    array.append(Info(currentline))

    output = tn.expect([b"VBAKBAT"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(currentline)
    result += currentline + "\n"
    array.append(Info(currentline))

    output = tn.expect([b"VREF"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(currentline)
    result += currentline + "\n"
    array.append(Info(currentline))

    output = tn.expect([b"VGND"], 10)
    output += tn.expect([b"\r\n"], 5)
    currentline = output[2].decode('ascii').replace("\b", "") + output[5].decode('ascii').replace("\b", "").replace("\r\n", "")
    print(currentline)
    result += currentline + "\n"
    array.append(Info(currentline))

    tn.close()

    return array


if __name__ == "__main__":
    if gethpadcinfo() == -1: raise Exception("gethpadcinfo fail")
