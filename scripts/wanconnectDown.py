"""
Example:
python wanconnectDown.py

wanconnect down

200 ok
"""
import os, sys, argparse, re

##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
    sys.path.append(p)
from calampCommon import initializetelnet, HOST
##########################################################################################


def wanconnectDown():
    try:
        print("\nwanconnect down")
        tn = initializetelnet(HOST)
        tn.write(b"wanconnect down")
        output = tn.expect([b"200 ok"], 10)
        print(output[2].decode('ascii').replace("b", ""))
        tn.close()
    except Exception as e:
        print("Received error: " + str(e))
        print("wanconnect down error")

if __name__ == "__main__":
    wanconnectDown()
